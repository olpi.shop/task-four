package com.olpi;

import java.util.Arrays;
import java.util.Random;
import java.util.StringJoiner;

public class MatrixManager {

    public boolean isSizeValid(String size) {
        try {
            return Integer.parseInt(size) > 0;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public boolean isIndexOfColumnValid(String indexOfColumn, int size) {
        try {
            return Integer.parseInt(indexOfColumn) > 0
                    && Integer.parseInt(indexOfColumn) <= size;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public void fillMatrix(int[][] matrix, int range) {

        Random random = new Random();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = random.nextInt(2 * range + 1) - range;
            }
        }
    }

    public String matrixToString(int[][] matrix) {

        StringJoiner joiner = new StringJoiner("");

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                joiner.add(String.format("%3d ", matrix[i][j]));
            }
            joiner.add("\n");
        }
        return joiner.toString();
    }

    public void sortMatrixByColumn(int[][] matrix, int column) {

        if (column > matrix.length || column < 1) {
            throw new IllegalArgumentException("Invalid column number!");
        }
        final int n = column;
        Arrays.parallelSort(matrix, (a, b) -> a[n - 1] - b[n - 1]);
    }

    public String findSumBetweenFirstAndSecondPositiveNumberInRow(
            int[][] matrix) {

        StringJoiner joiner = new StringJoiner("");

        for (int i = 0; i < matrix.length; i++) {

            int sum = 0;
            int firstElement = 0;
            int secondElement = 0;
            boolean isAdding = false;

            for (int j = 0; j < matrix[i].length; j++) {

                if (matrix[i][j] > 0 && isAdding) {
                    secondElement = matrix[i][j];
                    break;

                } else if (isAdding) {
                    sum += matrix[i][j];

                } else if (matrix[i][j] > 0) {
                    isAdding = true;
                    firstElement = matrix[i][j];
                }
            }

            if (firstElement != 0 && secondElement != 0) {
                joiner.add(i + 1 + ". Elements: " + firstElement + ", "
                        + secondElement + "; Sum: " + sum + ";\n");
            } else {
                joiner.add(i + 1 + ". There are no elements to add.\n");
            }
        }
        return joiner.toString();
    }

    public int[][] findMaxAndRemoveRowsAndColomnsContainingIt(int[][] matrix) {

        int[] indexesOfMaxValue = findIndexesOfMaxElementInMatrix(matrix);
        int maxI = indexesOfMaxValue[0];
        int maxJ = indexesOfMaxValue[1];

        int[][] newMatrix = new int[matrix.length - 1][matrix.length - 1];
        int iOfNewMatrix = 0;
        int jOfNewMatrix = 0;

        for (int i = 0; i < matrix.length; i++) {
            if (i == maxI) {
                continue;
            }
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == maxJ) {
                    continue;
                }
                newMatrix[iOfNewMatrix][jOfNewMatrix] = matrix[i][j];
                jOfNewMatrix++;
            }
            iOfNewMatrix++;
            jOfNewMatrix = 0;
        }
        return newMatrix;
    }

    private int[] findIndexesOfMaxElementInMatrix(int[][] matrix) {

        int max = matrix[0][0];
        int maxI = 0;
        int maxJ = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] > max) {
                    max = matrix[i][j];
                    maxI = i;
                    maxJ = j;
                }
            }
        }

        System.out.println("Maximum: " + max + "; Column: " + (maxJ + 1)
                + "; Row: " + (maxI + 1));

        return new int[] { maxI, maxJ };
    }

}
