package com.olpi;

import java.util.Scanner;

public class TaskFourApp {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        MatrixManager manager = new MatrixManager();
        String initialText = "";
        int size = 0;
        int[][] matrix;
        int range = 10;

        System.out.println("Please enter a size of matrix: "
                + "\nOr enter \"Exit\" to close the program.");

        while (!initialText.equalsIgnoreCase("exit")) {

            initialText = scanner.nextLine();

            if (manager.isSizeValid(initialText)) {
                size = Integer.valueOf(initialText);
                break;
            } else if (!initialText.equalsIgnoreCase("exit")) {
                System.out.println("You entered an invalid value, try again!");
            }
        }

        matrix = new int[size][size];
        if (!initialText.equalsIgnoreCase("exit")) {
            manager.fillMatrix(matrix, range);

            System.out.println(
                    "Initial matrix:\n" + manager.matrixToString(matrix));

            System.out.println("Please enter a column number to sort(from 1 to "
                    + size + "): \nOr enter \"Exit\" to close the program.");
        }

        int column = 0;
        while (!initialText.equalsIgnoreCase("exit")) {

            initialText = scanner.nextLine();

            if (manager.isIndexOfColumnValid(initialText, size)) {
                column = Integer.valueOf(initialText);
                break;
            } else if (!initialText.equalsIgnoreCase("exit")) {
                System.out.println("You entered an invalid value, try again!");
            }
        }

        if (!initialText.equalsIgnoreCase("exit")) {
            manager.sortMatrixByColumn(matrix, column);
            System.out.println(
                    "Sorted matrix:\n" + manager.matrixToString(matrix));
            System.out.println(manager
                    .findSumBetweenFirstAndSecondPositiveNumberInRow(matrix));
            matrix = manager.findMaxAndRemoveRowsAndColomnsContainingIt(matrix);
            System.out
                    .println("New matrix:\n" + manager.matrixToString(matrix));
        }

        scanner.close();

    }
}
